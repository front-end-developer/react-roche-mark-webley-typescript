# AUTHOR: MARK WEBLEY
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Vanilla Javascript version of this is here:
https://bitbucket.org/front-end-developer/react-roche-mark-webley/src/master/

### global install the following:
npm -g tslint typescript tslint-react

### `npm install`


Get up and running with:

`npm run build`

`npm run start`


#### TEST URLS:
http://localhost:3000/

http://localhost:3000/practitioner/3

## Project Explained

I placed a copy of your data.json file here on my personal
website wisdomer.co.uk, so I can play with it using redux-obervables and epics,
instead of doing it locally:
http://www.wisdomor.co.uk/exercises_1/patients/index.php

### Chart
I started building the chart in Plottable.js but realised YET GAIN, there is not enougth tutorial resources (I guess Palantir the creators want to monopolise on that fact as being the best service providers) to do what I want to do quickly, so I regretted I did not use plot.ly.js which would have been quicker & straight forward, then I ran out of time, then I realised perhapd it would have been quicker if I just done it all in D3 manually.

### PRACTITIONER (HOME) SCREEN
I have some code set up for the practitioner screen for typeahead but that works with the Epics,
but I never got time to finish the typeahead work.

### My Focus
I focused on the boostrapping the Redux, Reduc-Observer and the UI Styling and look and feel.

Then on the structure of the actions for redux and the epics etc.

Then the flow of data, deciding should I use redux payloads to send data to the chart of just past state to a chilld components props, so I took the quicker option.
I would prefer to use Redux, Acion and reducers then the data dispatched can be used with other widgets :).

So I focused on the architecture, the UI and the main functionalites.




## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).



### Redux sending payload to the store
Type & use paitent ID codes:

2588ac7f57fd9b49

2588ac7f57fd9b49

![Alt text](screenshots/search-for-patients-via-practitioner-id.jpg "Redux, Flux, Actions")

### Redux listen for changes to the store
![Alt text](screenshots/list-practitioners-paitents.jpg "Redux, Flux")
