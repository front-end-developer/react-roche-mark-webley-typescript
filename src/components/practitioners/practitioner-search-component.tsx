/**
 * Created by Mark Webley on 02/10/2019.
 */
import React, {Component} from "react";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {searchPractitionersActions, storePractitionerAction} from '../../redux/actions/searchPractitionersActions';

interface IProps {
    practitioner?: any,
        isLoading: bool,
        error?: any
}

interface IPractitioner {
    practitionerId?: null
}

interface IState {
    selectedPractitioner?: IPractitioner,
    success: boolean;
}

class PractitionerSearchComponent extends Component<IProps, IState> {

    static defaultProps = {
        selectedPractitioner: {
            practictionerId: null
        },
        success: false
    }

    constructor(props: IProps) {
        super(props);
        this.state = {
            selectedPractitioner: {
                practictionerId: null
            },
            success: false
        };
    }

    /**
     * @description validate the input data
     * @returns {boolean}
     */
    validatePractitionerId(practitionerId: string){
        let isValid = false;
        if (typeof practitionerId !== 'undefined' && practitionerId.length >= 16) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * @description for typeahead stuff
     * @param e ??? React.FormEvent<HTMLInputElement>
     */
    inputHandler = (e: any) => {
        this.setState({
            selectedPractitioner: {
                practitionerId: e.target.value
            }
        });
        if (this.validatePractitionerId(e.target.value)) {
            this.props.searchPractitionersActions(this.state.selectedPractitioner.practitionerId);
        }
    }

    // React.FormEvent<HTMLInputElement>
    submitSearch = (e: any) => {
        e.preventDefault();
        e.stopPropagation();
        let practitionerId = this.state.selectedPractitioner.practitionerId;
        if (this.validatePractitionerId(practitionerId) && this.props.success === true) {
            this.props.storePractitionerAction(practitionerId);
            // redirect too page....
            // redirect....line...

            // give time for the redux to trigger a litter or just use promises instead
            setTimeout(() => this.props.history.push(`/practitioner/${this.state.selectedPractitioner.practitionerId}`), 500);
        }
    }

    render() {
        return (
            <div>
                <div className="container">
                    <form className="card shadow-sm brand-panel-padding panel-top-line" onSubmit={this.submitSearch}>
                        <div className="card-body">
                            <h1>Buscar Patientes</h1>
                            <h3>Introduzca el ID del medico</h3>
                            <div className="form-group">
                                <label htmlFor="practitionerId">ID de medico
                                    <input type="text" onChange={this.inputHandler} className="form-control" id="practitionerId" placeholder="ID de medico" value={this.state.practitionerId} />
                                </label>
                            </div>
                            <button className="btn btn-primary btn-brand">Buscar</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        practitioner: state.searchPractitionerPayload.practitioner,
        success: state.searchPractitionerPayload.success,
        isLoading: state.searchPractitionerPayload.isLoading,
        error: state.searchPractitionerPayload.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        searchPractitionersActions: practitioner => dispatch(searchPractitionersActions(practitioner)),
        storePractitionerAction: practitioner => dispatch(storePractitionerAction(practitioner))
    }

};

export default connect(mapStateToProps, mapDispatchToProps)(PractitionerSearchComponent);
