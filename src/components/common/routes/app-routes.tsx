/**
 * Created by Mark Webley on 02/10/2019.
 */
import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import ListPatientsComponent from "../../patients/list-patients-component";
import PractitionerSearchComponent from "../../practitioners/practitioner-search-component";

const AppRoutes: React.FC = () => (
    <Router>
        <main>
            <Switch>
                <Route exact path='/' component={PractitionerSearchComponent}/>
                <Route path='/home' component={PractitionerSearchComponent}/>
                <Route exact path='/practitioner/:id' component={ListPatientsComponent}/>
            </Switch>
        </main>
    </Router>
);
export default AppRoutes;
