/**
 * Created by Mark Webley on 04/10/2019.
 */
import React from 'react';
import moment from 'moment';

export const PatientRowComponent: React.FC = ({fullname, dateOfBirth, diabetesType, ranges, timeBlocks, glucoseMesures, clickHandler}) => {
    let regExpString = /DIABETES_TYPE/gi;
    let typeOfDiabetes = diabetesType.replace(regExpString, 'tipo ');
    return (
        <tr>
            <td>{fullname}</td>
            <td>{moment(dateOfBirth).format("DD-MM-YYYY")}</td>
            <td>{typeOfDiabetes}</td>
            <td><button onClick={() => {
                clickHandler({
                    fullname,
                    data: {
                        dateOfBirth,
                        ranges,
                        timeBlocks,
                        glucoseMesures
                    }
                })
            }} className="btn btn-primary btn-brand btn-padding-small">Ver Perfil Del Paciente</button></td>
        </tr>
    )
}
