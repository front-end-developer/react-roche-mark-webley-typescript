import React from 'react';

export const LoaderComponent: React.FC = () => <div className="loader"><div className="spinner-border text-primary"><span className="sr-only">Loading Content...</span></div></div>;
