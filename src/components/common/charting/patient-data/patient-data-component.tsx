/**
 * Created by Mark Webley on 04/10/2019.
 */
import React, {Component} from "react";
import * as d3 from "d3";
import * as Plottable from 'plottable';
import moment from "moment/moment";
import style from './patient-data-component.module.scss';
//import d3 from 'd3';
//import Plottable from 'plottable';
// import $ from 'jquery';



//import Plottable from '../../../../../node_modules/plottable/plottable';

interface IProps {}

// TODO: do as seperate Model
interface ISelectedPatient {
    fullname: string,
    data: {
        dateOfBirth: string,
        ranges: any,
        timeBlocks: any,
        glucoseMesures: any
    }
}

// TODO: do as seperate Model
interface IState {
    practitionerId?: string,
    selectedPaitent?: ISelectedPatient
}

export class PatientDataComponent extends Component<IProps, IState> {

    // set scale
    // set xAxis

    static defaultProps = {
        selectedPaitent: {
            fullname: '',
            data: {
                dateOfBirth: '',
                ranges: '',
                timeBlocks: '',
                glucoseMesures: ''
            }
        }
    }

    constructor(props: IProps) {
        super(props);
    }

    /*
    const data = {
        selectedPaitent: {
                fullname,
                data: {
                    dateOfBirth,
                    ranges,
                    timeBlocks,
                    glucoseMesures
                }
            }
    }; // = this.props;
    */

    buildGraph() {
        /*
        const yScale = new Plottable.Scales.Category()
            .innerPadding(0)
            .outerPadding(0);
            */

        let dataMetric = {
            patientId: "5cc17bd0a75e3173228f1684",
                practitionerId: "2588ac7f57fd9b49",
            name: "Wilson",
            surname: "Wade",
            fullname: "Wilson Wade",
            dateOfBirth: "2005-11-13T04:11:36+00:00",
            diabetesType: "DIABETES_TYPE2",
            ranges: {
            low: 13,
                ideal: {
                from: 65,
                    to: 103
            },
            high: 247
        },
            timeBlocks: {
                night: {
                    from: "00:00",
                        to: "06:00"
                },
                breakfast: {
                    from: "06:00",
                        to: "12:00"
                },
                lunch: {
                    from: "12:00",
                        to: "18:00"
                },
                dinner: {
                    from: "18:00",
                        to: "00:00"
                }
            },
            glucoseMesures: [
                {
                    glucose: 85,
                    date: "2015-06-12T00:49:50+00:00"
                },
                {
                    glucose: 77,
                    date: "2015-06-12T07:09:03+00:00"
                },
                {
                    glucose: 58,
                    date: "2015-06-12T11:23:15+00:00"
                },
                {
                    glucose: 110,
                    date: "2015-06-12T16:00:58+00:00"
                },
                {
                    glucose: 97,
                    date: "2015-06-12T19:35:01+00:00"
                },
                {
                    glucose: 142,
                    date: "2015-06-12T21:15:50+00:00"
                }
            ]
        };

        var minDate = moment("2015-06-12T00:49:50+00:00").format("YYYY-MM-DD"); // "DD-MM-YYYY"
        var maxDate = moment("2015-06-12T21:15:50+00:00").format("YYYY-MM-DD");

        // TODO: to finish
  //      console.log(`year: ${moment("2015-06-12T00:49:50+00:00").year()} -  ${moment("2015-06-12T21:15:50+00:00").year()}`);
   //     console.log(`month: ${moment("2015-06-12T00:49:50+00:00").month()} -  ${moment("2015-06-12T21:15:50+00:00").month()}`);
   //     console.log(`date: ${moment("2015-06-12T00:49:50+00:00").date()} -  ${moment("2015-06-12T21:15:50+00:00").date()}`);

        var minDate = `${moment("2015-06-12T00:49:50+00:00").year()}, ${moment("2015-06-12T00:49:50+00:00").month()}, ${moment("2015-06-12T00:49:50+00:00").date()}`;
        var maxDate = `${moment("2015-06-12T21:15:50+00:00").year()}, ${moment("2015-06-12T21:15:50+00:00").month()}, ${moment("2015-06-12T21:15:50+00:00").date()}`;
        console.log(`${minDate} -  ${maxDate}`);


        var yScale = new Plottable.Scales.Linear();
        var xScale = new Plottable.Scales.Time();
            //.domain([new Date(minDate)]);

            // .domain([new Date(2015, 0, 1), new Date(2015, 11, 31)]);

        // moment("2015-06-12T11:23:15+00:00").format("DD-MM-YYYY"), moment("2015-06-12T21:15:50+00:00").format("DD-MM-YYYY")
        // new Date(1, 0, 2015), new Date(31, 11, 2015)

        var yAxis = new Plottable.Axes.Numeric(yScale, "left");
        var xAxis = new Plottable.Axes.Time(xScale, "bottom");

        var plot = new Plottable.Plots.Line();
        plot.x(function(d: any) { return d.x; }, xScale);
        plot.y(function(d: any) { return d.y; }, yScale);
            //.range([moment("2015-06-12T00:49:50+00:00").format("DD-MM-YYYY"), moment("2015-06-12T21:15:50+00:00").format("DD-MM-YYYY")]);

        /*
        var data = [
            { "x": 0, "y": 1 },
            { "x": 1, "y": 2 },
            { "x": 2, "y": 4 },
            { "x": 3, "y": 8 }
        ];
        */

        var data = [
            {
                y: 85,
                x: moment("2015-06-12T00:49:50+00:00").format("X")
            },
            {
                y: 77,
                x: moment("2015-06-12T07:09:03+00:00").format("X")
            },
            {
                y: 58,
                x: moment("2015-06-12T11:23:15+00:00").format("X")
            },
            {
                y: 110,
                x: moment("2015-06-12T16:00:58+00:00").format("X")
            },
            {
                y: 97,
                x: moment("2015-06-12T19:35:01+00:00").format("X")
            },
            {
                y: 142,
                x: moment("2015-06-12T21:15:50+00:00").format("X")
            }
        ];

        var dataset = new Plottable.Dataset(data);
        plot.addDataset(dataset);

        const config = {
            colorDomain: ["MISSING_DATA", "DATA_OUT_OF_RANGE_(HIGH)", "DATA_OUT_OF_RANGE_(LOW)", "FULL_DATA"]
        };

        var colorScale = new Plottable.Scales.Color()
            .domain(config.colorDomain)
            .range([d3.rgb(200, 200, 200), d3.rgb(250, 212, 25), d3.rgb(196, 0, 1), d3.rgb(99, 194, 97)]);

        var legend = new Plottable.Components.Legend(colorScale)
            .maxEntriesPerRow(10);

        var chart = new Plottable.Components.Table([
            [yAxis, plot],
            [null, xAxis] // ,
            // [null, legend]
        ]);

        chart.renderTo(".patient-data");

    }

    componentDidMount() {
        setTimeout(() => this.buildGraph(), 1000);
    }

    render() {
        return (
            <div className='container chart'>
                <div className={`{card shadow-sm ${style.cardChart}}`}>
                    <div className="card-body">
                        <div className={`{patient-data ${style.chart}}`}>Chart Test</div>
                    </div>
                </div>
            </div>
        )
    }
};
