/**
 * Created by Mark Webley on 02/10/2019.
 */
import React, {Component} from "react";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {LoaderComponent} from '../common/ui/loaders/loader';
import {PatientRowComponent} from '../common/ui/table-rows/patient-row-component';
import {PatientDataComponent} from '../common/charting/patient-data/patient-data-component';
import {loadPatientsAction, loadPatientSuccessAction} from '../../redux/actions/loadPatientsActions';

interface IProps {
    practitioner?: any,
    patients: any,
    isLoading: bool,
    error?: any
}

// TODO: do as seperate Model
interface ISelectedPatient {
    fullname: string,
    data: {
        dateOfBirth: string,
        ranges: any,
        timeBlocks: any,
        glucoseMesures: any
    }
}

// TODO: do as seperate Model
interface IState {
        practitionerId?: string,
        selectedPaitent?: ISelectedPatient
}

class ListPatientsComponent extends Component<IProps, IState> {
    state: IState;
    static defaultProps = {
        practitionerId: '',
        selectedPaitent: {
            fullname: '',
            data: {
                dateOfBirth: '',
                ranges: '',
                timeBlocks: '',
                glucoseMesures: ''
            }
        }
    }

    constructor(props: IProps) {
        super(props);
    }

    componentDidMount() {
        this.props.loadPatientsAction();
    }

    showDataVisual = (data: any) => {
        console.log('display chart and send redux action with payload', JSON.stringify(data, null, 7));
        this.setState({
            selectedPaitent: {
                fullname: data,
                data: {
                    dateOfBirth: data.dateOfBirth,
                    ranges: data.ranges,
                    timeBlocks: data.timeBlocks,
                    glucoseMesures: data.glucoseMesures
                }
            }
        });
    }

    public render() {
        const {
            loadPatientsAction,
            isLoading,
            error,
            practitioner,
            patients
        } = this.props;
       const practitionerId = practitioner; // '2588ac7f57fd9b49';
       const practitionersPatients = (patients.length === 0) ? null : patients.filter(patient => patient.practitionerId === practitionerId);

        return (
            <div>
                <div>
                    {isLoading && <LoaderComponent />}
                    {error && <h1>Loading Error: {error}</h1>}
                </div>

                <section>
                    <PatientDataComponent {...this.state.selectedPaitent} />
                </section>

                <div className="container">
                    <div className="card shadow-sm">
                        <div className="card-body">
                            <i className="fa fa-refresh btn-brand-refresh" onClick={loadPatientsAction}></i>
                            <table id="example" className="table">
                                <thead className="background-skyblue table-borderless">
                                    <tr>
                                        <th>Nombre del paciente</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Tipo de diabetes</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        (practitionersPatients === null) ? null
                                            : practitionersPatients.map(patient => (<PatientRowComponent key={patient.patientId} clickHandler={this.showDataVisual} {...patient} />))
                                    }

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        practitioner: state.searchPractitionerPayload.practitioner,
        patients: state.loadPatientsPayload.patients,
        isLoading: state.loadPatientsPayload.isLoading,
        error: state.loadPatientsPayload.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        loadPaitentsSuccessAction: patients => dispatch(loadPatientSuccessAction(patients)),
        loadPatientsAction: () => dispatch(loadPatientsAction())
    }

};

export default connect(mapStateToProps, mapDispatchToProps)(ListPatientsComponent);
