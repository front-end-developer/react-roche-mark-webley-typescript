import React from 'react';
import AppContainer from './components/app-container/app-container';
import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
        <AppContainer />
    </div>
  );
}

export default App;
