/**
 * Created by Mark Webley on 02/10/2019.
 */
import * as types from '../actions/allActionTypes';
export default function chartReducer(state = [], action) {
    let newState;
    switch (action.type) {
        case types.CHART_ONE_SELECTED_DATA:
            newState = ({...state, ...action.payload}); // [...state, {...action.payload}]; // ({...state,...action.payload});
            break;
        default:
            return state;
    }
    return newState;
}
