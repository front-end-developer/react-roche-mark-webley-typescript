/**
 * Created by Mark Webley on 02/10/2019.
 */
import {combineReducers} from 'redux';
import { loadingAll } from 'redux-global-loader';
import loadPatientsPayload from './loadPatientsReducer';
import searchPractitionerPayload from './searchPractitionerPayload';
import payload from './chartReducer'; // change this name from payload to be specific to the chart | how do we define this as a convention


const rootReducer = combineReducers({
    loadingAll,
    payload,
    loadPatientsPayload,
    searchPractitionerPayload
});

export default rootReducer;
