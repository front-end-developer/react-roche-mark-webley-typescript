/**
 * Created by Mark Webley on 04/10/2019.
 */
import * as types from "../actions/allActionTypes";

interface IInitialState {
    practitioner: string,
    isLoading: boolean,
    error?: null
}

const initialState = {
    practitioner: '',
    isLoading: false,
    error: null
}

export default function searchPractitionerPayload(state:IInitialState = initialState, action) {
    let newState;
    switch (action.type) {
        case types.SEARCH_PRACTITIONAR:
            newState = {
                ...state,
                isLoading: true,
                error: null,
            };
            break;
        case types.LOAD_PRACTITIONAR_SUCCESS:
            newState = {
                ...state,
                practitioner: action.practitioner,
                success: true,
                isLoading: false,
                error: null
            }
            break;
        case types.STORE_PRACTITIONAR:
            newState = {
                practitioner: action.practitioner,
                success: true,
                isLoading: false,
                error: null
            }
            break;
        case types.LOAD_PRACTITIONAR_ERROR:
            newState = {
                practitioner: '',
                success: false,
                isLoading: false,
                error: action.error
            }
            break;
        default:
            return state;
    }
    return newState;
}
