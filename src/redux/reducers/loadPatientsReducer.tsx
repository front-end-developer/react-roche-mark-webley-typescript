/**
 * Created by Mark Webley on 03/10/2019.
 */
import * as types from '../actions/allActionTypes';

// loadPatientsReducer

const initialState = {
    patients: [],
    isLoading: false,
    error: null
}

export default function loadPatientsReducer(state = initialState, action) {
    let newState;
    switch (action.type) {
        case types.LOAD_PATIENTS:
            newState = {
                ...state,
                isLoading: true,
                error: null,
            };
            break;
        case types.LOAD_SUCCESS:
            newState = {
                patients: [...action.patients],
                isLoading: false,
                error: null
            }
            break;
        case types.LOAD_ERROR:
            newState = {
                patients: [],
                isLoading: false,
                error: action.error
            }
            break;
        default:
            return state;
    }
    return newState;
}
