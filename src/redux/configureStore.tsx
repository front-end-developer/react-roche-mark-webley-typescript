/**
 * Created by Mark Webley on 02/10/2019.
 */
import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from './reducers/rootReducer';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import { createEpicMiddleware } from 'redux-observable';
import promise from 'redux-promise-middleware';
import { globalLoaderMiddleware }  from 'redux-global-loader';
import { rootEpic } from './epics';

console.log('NODE_ENV env:', process.env.NODE_ENV);
console.log('BABEL_ENV env:', process.env.BABEL_ENV );

const epicMiddleware = createEpicMiddleware();
const devMiddleware = () => {
    return applyMiddleware(
        reduxImmutableStateInvariant(),
        promise,
        globalLoaderMiddleware,
        epicMiddleware
    )
}
const prodMiddleware = () => {
    return applyMiddleware(
        promise,
        globalLoaderMiddleware,
        epicMiddleware
    )
}


export default function configureStore(initialState?) {
    /**
     * NOTES
     * window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ adds support for Redux dev tools,
     * in production we will switch this
     *
     * WHY: redux-immutable-state-invariant (urgent: should only ve used in dev and not production).
     * This is a Redux middleware that spits an error on you when you try to mutate
     * your state either inside a dispatch or between dispatches.
     * As said above, don't use this in production! It involves a lot of object copying and will
     * degrade your app's performance. This is intended to be a tool to aid you in development
     * and help you catch bugs.
     */
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const storeCreated = createStore(
        rootReducer,
        initialState,
        (process.env.NODE_ENV === 'development' ? composeEnhancers(devMiddleware()) : prodMiddleware())
    );

    epicMiddleware.run(rootEpic);
    return storeCreated;
}
