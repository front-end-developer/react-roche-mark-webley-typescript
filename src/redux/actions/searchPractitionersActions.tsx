/**
 * Created by Mark Webley on 04/10/2019.
 */
import * as types from './allActionTypes';

const searchPractitionersActions = (practitioner: any) => {
    return {
        type: types.SEARCH_PRACTITIONAR,
        practitioner,
        error: ''
    };
}

const storePractitionerAction = (practitioner: any) => {
    return {
        type: types.STORE_PRACTITIONAR,
        practitioner,
        error: ''
    };
}

export {
    searchPractitionersActions,
    storePractitionerAction
}

