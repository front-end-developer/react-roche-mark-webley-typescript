/**
 * Created by Mark Webley on 03/10/2019.
 */
import * as types from './allActionTypes';
const loadPatientsAction = () => {
    return {
        type: types.LOAD_PATIENTS,
        error: ''
    };
}

const loadPatientSuccessAction = (patients: any) => {
    return {
        type: types.LOAD_SUCCESS,
        patients,
        error: ''
    };
}

const loadPatientsErrorAction = (message: any) => {
    return {
        type: types.LOAD_ERROR,
        error: message
    };
}

export {
    loadPatientsAction,
    loadPatientSuccessAction,
    loadPatientsErrorAction
}
